package testCase;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

import pages.HomePage;
import pages.LoginPage;

public class EditTaskTestCase extends ProjectSpecificImplementation {
	@BeforeTest
	public void setFileName() {
		excelFileName = "EditTask";
		testCaseName = "Edit Task TestCase";
		testDescription = "Edit Task on Salesforce";
		nodes = "Task";		
		category = "Smoke";
		authors = "Priscilla";
	}
	
	@Test(dataProvider = "fetchData")
	public void editTask(String subjectName) throws InterruptedException, IOException {
		new LoginPage(driver, node)
			.enterUsername("makaia@testleaf.com") 
			.enterPassword("BootcampSel$123")
			.clickLoginButton()
			.clickToggleMenu()
			.clickViewAll()
			.clickSales()
			.clickTasksTab()
			.clickRecentlyViewed()
			.search(subjectName)
			.clickArrowButton()
			.clickEditDropDown()
			.clickDueDate()
			.clickDueDateToday()
			;

			//rework required

	}

}
