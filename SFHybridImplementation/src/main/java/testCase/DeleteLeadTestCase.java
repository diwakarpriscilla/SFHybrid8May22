package testCase;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

import pages.HomePage;
import pages.LoginPage;

public class DeleteLeadTestCase extends ProjectSpecificImplementation {
	
	@BeforeTest
	public void setFileName() {
		excelFileName = "DeleteLead";
		testCaseName = "Delete Lead TestCase";
		testDescription = "Delete Lead on Salesforce";
		nodes = "Lead";		
		category = "Smoke";
		authors = "Priscilla";
	}
	
	@Test(dataProvider = "fetchData")
	public void deleteLead(String lName, String lead_Name) throws InterruptedException, IOException {
		new LoginPage(driver, node)
			.enterUsername("makaia@testleaf.com") 
			.enterPassword("BootcampSel$123")
			.clickLoginButton()
			.clickToggleMenu()
			.clickViewAll()
			.clickSales()
			.clickLeadTab()
			.searchLeads(lName)
			.clickDropdown()
			.clickDeleteLead()
			.clickDeleteButton()
			.verifyDeletion(lead_Name);		
		
	}

}
