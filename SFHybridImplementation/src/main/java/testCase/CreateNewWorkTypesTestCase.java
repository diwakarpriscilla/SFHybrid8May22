package testCase;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

import pages.HomePage;
import pages.LoginPage;

public class CreateNewWorkTypesTestCase extends ProjectSpecificImplementation {
	
	  @BeforeTest 
	  public void setFileName() { 
		  excelFileName = "CreateWorkTypes"; 
		  testCaseName = "Create Work Type TestCase";
		  testDescription = "Create Work Type on Salesforce";
		  nodes = "Worktype";		
		  category = "Smoke";
		  authors = "Priscilla";
		}
	 
	
	@Test(dataProvider = "fetchData")
	public void createWorkTypes(String WorkTypeName, String Description, String Shift, String Duration) throws InterruptedException, IOException {
		new LoginPage(driver, node)
			.enterUsername("makaia@testleaf.com") 
			.enterPassword("BootcampSel$123")
			.clickLoginButton()
			.clickToggleMenu()
			.clickViewAll()
			.clickWorkTypes()
			.clickNewButton()
			.typeWorkTypeName(WorkTypeName)
			.typeDescription(Description)
			.newOperatingHours(Shift)
			.enterEstimatedDuration(Duration)
			.clickSave()
			.verifyCreation(WorkTypeName);
	}

}
