package testCase;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

import pages.HomePage;
import pages.LoginPage;

public class DeleteAccountTestCase extends ProjectSpecificImplementation {
	
	@BeforeTest
	public void setFileName() {
		excelFileName = "DeleteAccount";
		testCaseName = "DeleteAccountTestCase";
		testDescription = "Delete Account on Salesforce";
		nodes = "Account";		
		category = "Smoke";
		authors = "Priscilla";
	}
	
	@Test(dataProvider = "fetchData")
	public void deleteAccount(String acctName) throws InterruptedException, IOException {
		new LoginPage(driver, node)
			.enterUsername("makaia@testleaf.com") 
			.enterPassword("BootcampSel$123")
			.clickLoginButton()
			.clickToggleMenu()
			.clickViewAll()
			.clickSales()
			.clickAccountTab()
			.searchAccount(acctName)
			.countAccount(acctName)
			.clickDropdown()
			.selectDeleteDropdown()
			.clickDeleteButton()
			.countAccount(acctName)
			.verification();
			
		
	}

}
