package testCase;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

import pages.HomePage;
import pages.LoginPage;

public class CreateCaseMandatoryTestCase extends ProjectSpecificImplementation {
	@BeforeTest
	public void setFileName() {
		excelFileName = "CreateCaseMandatory";
		testCaseName = "CreateCaseTestCase";
		testDescription = "Create a new Case on Salesforce";
		nodes = "Case";		
		category = "Smoke";
		authors = "Priscilla";
	}
	
	@Test(dataProvider = "fetchData")
	public void createCaseMandatory(String contact, String Subject, String Description, String errorMessageExpected) throws InterruptedException, IOException {
		new LoginPage(driver, node)
			.enterUsername("makaia@testleaf.com") 
			.enterPassword("BootcampSel$123")
			.clickLoginButton()
			.clickToggleMenu()
			.clickViewAll()
			.clickSales()
			.showMore()
			.clickNewButton()
			.typeContact(contact)
			.selectNone()
			.enterSubject(Subject)
			.enterDescription(Description)
			.clickSave()
			.verifyErrorMessage(errorMessageExpected);
	}

}
