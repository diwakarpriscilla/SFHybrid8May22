package testCase;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

import pages.HomePage;
import pages.LoginPage;

public class CreateContactTestCase extends ProjectSpecificImplementation {
	
	@BeforeTest
	public void setFileName() {
		excelFileName = "CreateContact";
		testCaseName = "CreateContactTestCase";
		testDescription = "Create a new Contact on Salesforce";
		nodes = "Contact";		
		category = "Smoke";
		authors = "Priscilla";
	}
	
	@Test(dataProvider = "fetchData")
	public void createContact(String FirstName, String Surname, String Email, String AccountName) throws InterruptedException, IOException {
		new LoginPage(driver, node)
			.enterUsername("makaia@testleaf.com") 
			.enterPassword("BootcampSel$123")
			.clickLoginButton()
			.clickSVG()
			.clickNewContact()
			.selectSalutation()
			.enterFirstname(FirstName)
			.enterLastname(Surname)
			.enterEmail(Email)
			.searchAccount()
			.newAccount()
			.enterAccountName(AccountName)
			.clickAccountSave()
			.verifyAccountSave(AccountName)
			.clickSaveContact()
			.verifyContactSave(FirstName, Surname);
		
	}

}
