package testCase;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

import pages.HomePage;
import pages.LoginPage;

public class CreateLeadMandatoryTestCase extends ProjectSpecificImplementation {
	@BeforeTest
	public void setFileName() {
		excelFileName = "CreateLeadMandatory";
		testCaseName = "CreateLeadMandatoryTestCase";
		testDescription = "Create Lead with Mandatrory on Salesforce";
		nodes = "Lead";		
		category = "Smoke";
		authors = "Priscilla";
	}
	
	@Test(dataProvider = "fetchData")
	public void createLeadMandatory(String companyName, String fName, String expectedResult) throws InterruptedException, IOException {
		new LoginPage(driver, node)
			.enterUsername("makaia@testleaf.com") 
			.enterPassword("BootcampSel$123")
			.clickLoginButton()
			.clickToggleMenu()
			.clickViewAll()
			.clickSales()
			.clickLeadTab()
			.clickNewButton()
			.selectSalutation()
			.enterCompanyName(companyName)
			.enterFirstName(fName)
			.clickSave()
			.mandatoryFieldVerification(expectedResult);
	}

}
