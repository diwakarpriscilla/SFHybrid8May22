package testCase;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

import pages.HomePage;
import pages.LoginPage;

public class EditContactTestCase extends ProjectSpecificImplementation {
	
	@BeforeTest
	public void setFileName() {
		excelFileName = "EditContact";
		testCaseName = "EditContactTestCase";
		testDescription = "Edit a Contact on Salesforce";
		nodes = "Contact";		
		category = "Smoke";
		authors = "Priscilla";
	}
	
	@Test(dataProvider = "fetchData")
	public void editContact(String contactName, String titleName, String phone) throws InterruptedException, IOException {
		new LoginPage(driver, node)
			.enterUsername("makaia@testleaf.com") 
			.enterPassword("BootcampSel$123")
			.clickLoginButton()
			.clickToggleMenu()
			.clickViewAll()
			.clickContacts()
			.sizeOfContacts()
			.searchContacts(contactName)
			.clickDropdown()
			.clickEditDropDown()
			.enterTitle(titleName)
			.enterBirthday()
			.enterLeadSource()
			.enterPhone(phone)
			.clickSave()
			.verifySave(contactName)
			.searchContacts(contactName)
			.verifyPhoneNumber(phone)
			;
			
		
	}

}
