package testCase;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

import pages.HomePage;
import pages.LoginPage;

public class CreateNewTaskTestCase extends ProjectSpecificImplementation {
	@BeforeTest
	public void setFileName() {
		excelFileName = "CreateTask";
		testCaseName = "Create Task TestCase";
		testDescription = "Create Task on Salesforce";
		nodes = "Task";		
		category = "Smoke";
		authors = "Priscilla";
	}
	
	@Test(dataProvider = "fetchData")
	public void createTask(String subjectName, String contactName) throws InterruptedException, IOException {
		new LoginPage(driver, node)
			.enterUsername("makaia@testleaf.com") 
			.enterPassword("BootcampSel$123")
			.clickLoginButton()
			.clickToggleMenu()
			.clickViewAll()
			.clickSales()
			.clickTasksTab()
			.clickTasksTab()
			.clickNewTask()
			.enterSubject(subjectName)
			.enterContact(contactName)
			.clickStatus()
			.clickStatusOptions()
			.clickSave()
			.verifyCreation(subjectName);

	}

}
