package testCase;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

import pages.HomePage;
import pages.LoginPage;

public class EditAccountTestCase extends ProjectSpecificImplementation {
	
	@BeforeTest
	public void setFileName() {
		excelFileName = "EditAccount";
		testCaseName="EditAccountTestCase";
		testDescription="Edit Account on Salesforce";
		nodes = "Account";		
		category="Smoke";
		authors="Priscilla";
	}
	
	@Test(dataProvider = "fetchData")
	public void editAccount(String UserName, String Password, String acctName, String Billing_Address, String Billing_City, String Billing_Province, 
			String Billing_Postalcode, String Billing_Country, String Shipping_Address, String Shipping_City, 
			String Shipping_Province, String Shipping_Postalcode, String Shipping_Country, String phone) throws InterruptedException, IOException {
		new LoginPage(driver, node)
			.enterUsername(UserName) 
			.enterPassword(Password)
			.clickLoginButton()
			.clickToggleMenu()
			.clickViewAll()
			.clickSales()
			.clickAccountTab()
			.searchAccount(acctName)
			.clickDropdown()
			.selectEdit()
			.selectType()
			.selectIndustry()
			.billingAddress(Billing_Address, Billing_City, Billing_Province, Billing_Postalcode, Billing_Country)
			.shippingAddress(Shipping_Address, Shipping_City, Shipping_Province, Shipping_Postalcode, Shipping_Country)
			.selectCustomerPriority()
			.clickSLA()
			.selectActive()
			.selectUpsellOppurtunity()
			.enterPhoneNumber(phone)
			.saveDetails()
			.verifyPhoneNumber(phone);
		
	}

}
