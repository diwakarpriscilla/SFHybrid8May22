package testCase;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

import pages.HomePage;
import pages.LoginPage;

public class CreateAccountTestCase extends ProjectSpecificImplementation {
	
	@BeforeTest
	public void setFileName() {
		excelFileName = "CreateAccount";
		testCaseName = "CreateAccountTestCase";
		testDescription = "Create a new Account on Salesforce";
		nodes = "Account";		
		category = "Smoke";
		authors = "Priscilla";
	}
	
	@Test(dataProvider = "fetchData")
	public void createAccount(String UserName, String Password, String acctName) throws InterruptedException, IOException {
		new LoginPage(driver, node)
			.enterUsername(UserName) 
			.enterPassword(Password)
			.clickLoginButton()
			.clickToggleMenu()
			.clickViewAll()
			.clickSales()
			.clickAccountTab()
			.clickNewButton()
			.enterAccountName(acctName)
			.selectOwnership()
			.clickSave()
			.verifyAccountName(acctName);
		
	}

}
