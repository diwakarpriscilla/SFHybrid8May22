package testCase;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

import pages.HomePage;
import pages.LoginPage;

public class CreateNewLeadTestCase extends ProjectSpecificImplementation {
	@BeforeTest
	public void setFileName() {
		excelFileName = "CreateLead";
		testCaseName = "Create Lead TestCase";
		testDescription = "Create Lead on Salesforce";
		nodes = "Lead";		
		category = "Smoke";
		authors = "Priscilla";
	}
	
	@Test(dataProvider = "fetchData")
	public void createLead(String companyName, String LName) throws InterruptedException, IOException {
		new LoginPage(driver, node)
			.enterUsername("makaia@testleaf.com") 
			.enterPassword("BootcampSel$123")
			.clickLoginButton()
			.clickToggleMenu()
			.clickViewAll()
			.clickSales()
			.clickLeadTab()
			.clickNewButton()
			.selectSalutation()
			.enterCompanyName(companyName)
			.enterLastname(LName)
			.clickSave()
			.verification(LName);
	}

}
