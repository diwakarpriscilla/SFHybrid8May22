package testCase;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

import pages.HomePage;
import pages.LoginPage;

public class DeleteWorkTypesTestCase extends ProjectSpecificImplementation {
	
	  @BeforeTest 
	  public void setFileName() { 
		  excelFileName = "DeleteWorkTypes"; 
		  testCaseName = "Delete Work Type TestCase";
		  testDescription = "Delete Work Type on Salesforce";
		  nodes = "Worktype";		
		  category = "Smoke";
		  authors = "Priscilla";
		  }
	 
	
	@Test(dataProvider = "fetchData")
	public void deleteWorkTypes(String WorkTypeName) throws InterruptedException, IOException {
		new LoginPage(driver, node)
			.enterUsername("makaia@testleaf.com") 
			.enterPassword("BootcampSel$123")
			.clickLoginButton()
			.clickToggleMenu()
			.clickViewAll()
			.clickWorkTypes()
			.searchWorkType(WorkTypeName)
			.clickArrowButton()
			.clickDeleteDropDown()
			.clickDeleteButton()
			.verifyDelete(WorkTypeName);
	}

}
