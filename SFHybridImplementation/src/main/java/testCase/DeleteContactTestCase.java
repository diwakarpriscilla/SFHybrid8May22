package testCase;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

import pages.HomePage;
import pages.LoginPage;

public class DeleteContactTestCase extends ProjectSpecificImplementation {
	
	@BeforeTest
	public void setFileName() {
		excelFileName = "DeleteContact";
		testCaseName = "DeleteContactTestCase";
		testDescription = "Delete a Contact on Salesforce";
		nodes = "Contact";		
		category = "Smoke";
		authors = "Priscilla";
	}
	
	@Test(dataProvider = "fetchData")
	public void deleteContact(String contactName) throws InterruptedException, IOException {
		new LoginPage(driver, node)
			.enterUsername("makaia@testleaf.com") 
			.enterPassword("BootcampSel$123")
			.clickLoginButton()
			.clickToggleMenu()
			.clickViewAll()
			.clickContacts()
			.sizeOfContacts()
			.searchContacts(contactName)
			.clickDropdown()
			.clickDeletetDropDown()
			.clickDeleteButton()
			
			;
			
		
	}

}
