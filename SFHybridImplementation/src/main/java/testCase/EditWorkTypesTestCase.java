package testCase;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

import pages.HomePage;
import pages.LoginPage;

public class EditWorkTypesTestCase extends ProjectSpecificImplementation {
	
	  @BeforeTest 
	  public void setFileName() { 
		  excelFileName = "EditWorkTypes";
		  testCaseName = "Edit Work Type TestCase";
		  testDescription = "Edit Work Type on Salesforce";
		  nodes = "Worktype";		
		  category = "Smoke";
		  authors = "Priscilla";
		  }
	 
	
	@Test(dataProvider = "fetchData")
	public void editWorkTypes(String WorkTypeName, String StartTime, String EndTime) throws InterruptedException, IOException {
		new LoginPage(driver, node)
			.enterUsername("makaia@testleaf.com") 
			.enterPassword("BootcampSel$123")
			.clickLoginButton()
			.clickToggleMenu()
			.clickViewAll()
			.clickWorkTypes()
			.searchWorkType(WorkTypeName)
			.clickArrowButton()
			.clickEditDropDown()
			.enterTimeFrameStart(StartTime)
			.enterTimeFramEnd(EndTime)
			.clickSave()
			.verifyEdit(WorkTypeName);
	}

}
