package testCase;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

import pages.HomePage;
import pages.LoginPage;

public class EditLeadTestCase extends ProjectSpecificImplementation {
	
	@BeforeTest
	public void setFileName() {
		excelFileName = "EditLead";
		testCaseName = "Edit Lead TestCase";
		testDescription = "Edit Lead on Salesforce";
		nodes = "Lead";		
		category = "Smoke";
		authors = "Priscilla";
	}
	
	@Test(dataProvider = "fetchData")
	public void editLead(String lName, String fName) throws InterruptedException, IOException {
		new LoginPage(driver, node)
			.enterUsername("makaia@testleaf.com") 
			.enterPassword("BootcampSel$123")
			.clickLoginButton()
			.clickToggleMenu()
			.clickViewAll()
			.clickSales()
			.clickLeadTab()
			.searchLeads(lName)
			.clickDropdown()
			.clickEditDropDown()
			.enterFirstName(fName)
			.selectLeadStatus()
			.clickSave()
			.verifyResult(fName, lName);		
		
	}

}
