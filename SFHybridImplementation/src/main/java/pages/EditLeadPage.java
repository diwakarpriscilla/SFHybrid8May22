package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;
import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

public class EditLeadPage extends ProjectSpecificImplementation{
	
	public EditLeadPage(RemoteWebDriver driver,ExtentTest node) {
		this.driver = driver;
		this.node = node;
	}
	
	public EditLeadPage enterFirstName(String fName) throws InterruptedException {		
	clearAndType((locateElementProp("EditLeadPage.FirstName.Xpath")),fName);
	Thread.sleep(2000);
	return this;
	}
			
	public EditLeadPage selectLeadStatus() throws InterruptedException {
	executorClick("EditLeadPage.LeadStatus.Xpath");
	executorClick("EditLeadPage.LeadStatusWorking.Xpath");
	return this;
	}
	
	public RecentlyViewedLeadPage clickSave() {
	click(locateElementProp("EditLeadPage.SaveEdit.Xpath"));
	return new RecentlyViewedLeadPage(driver, node);
	}
	
	
}