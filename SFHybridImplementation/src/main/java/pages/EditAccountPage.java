package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;
import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

public class EditAccountPage extends ProjectSpecificImplementation {
	
	public EditAccountPage(RemoteWebDriver driver, ExtentTest node) {
		this.driver = driver;
		this.node = node;
	}
	
	public EditAccountPage selectType() throws InterruptedException { 
    executorClick("EditAccountPage.Type.Xpath");
    executorClick("EditAccountPage.TechPart.Xpath");
    Thread.sleep(2000);
    return this;
	}
    
	public EditAccountPage selectIndustry() throws InterruptedException {
    executorClick("EditAccountPage.Industry.Xpath");
    executorClick("EditAccountPage.HealthCare.Xpath");
    Thread.sleep(2000);
    return this;
	}
       
	public EditAccountPage billingAddress(String Billing_Address, String Billing_City, String Billing_Province, String Billing_Postalcode, String Billing_Country){
	clearAndType(locateElement("xpath","//div/textarea[@name='street']"),Billing_Address);
	clearAndType(locateElementProp("EditAccountPage.City.Xpath"),Billing_City);
	clearAndType(locateElementProp("EditAccountPage.Province.Xpath"),Billing_Province);
	clearAndType(locateElementProp("EditAccountPage.PostalCode.Xpath"),Billing_Postalcode);	
	clearAndType(locateElementProp("EditAccountPage.Country.Xpath"),Billing_Country);
    return this;
	}
    	    
	public EditAccountPage shippingAddress(String Shipping_Address, String Shipping_City, String Shipping_Province, String Shipping_Postalcode, String Shipping_Country) throws InterruptedException{ 
    clearAndType(locateElementProp("EditAccountPage.ShippingAddress.Xpath"),Shipping_Address);
    clearAndType(locateElementProp("EditAccountPage.ShippingProvince.Xpath"),Shipping_Province);
    clearAndType(locateElementProp("EditAccountPage.ShippingPostalCode.Xpath"),Shipping_Postalcode);
    clearAndType(locateElementProp("EditAccountPage.ShippingCountry.Xpath"),Shipping_Country);
    Thread.sleep(2000);
    return this;
	}
    
    public EditAccountPage selectCustomerPriority() throws InterruptedException{ 
    executorClick("EditAccountPage.CustomerPriority.Xpath"); 
    executorClick("EditAccountPage.LowPriority.Xpath");
    Thread.sleep(2000);
    return this;
	}
    
    public EditAccountPage clickSLA() throws InterruptedException {
    executorClick("EditAccountPage.SLA.Xpath");
    executorClick("EditAccountPage.Silver.Xpath");
    Thread.sleep(2000);
    return this;
    }
    
    public EditAccountPage selectActive() throws InterruptedException {
    click(locateElementProp("EditAccountPage.Active.Xpath"));
    executorClick("EditAccountPage.ActiveNo.Xpath");
    Thread.sleep(2000);
    return this;
    }
    
    public EditAccountPage selectUpsellOppurtunity() {
    click(locateElementProp("EditAccountPage.UpsellOpportunity.Xpath"));
    executorClick("EditAccountPage.UpsellNo.Xpath");
    return this;
	}
    
    public EditAccountPage enterPhoneNumber(String phone) throws InterruptedException {
    clearAndType(locateElementProp("EditAccountPage.Phone.Xpath"),phone);
    appendEnter(locateElementProp("EditAccountPage.Phone.Xpath"));
    Thread.sleep(2000);
    return this;
    }
    
    public RecentlyViewedAccountPage saveDetails() throws InterruptedException
    {
    click(locateElementProp("EditAccountPage.SaveEdit.Xpath"));
    Thread.sleep(2000);
    return new RecentlyViewedAccountPage(driver, node);
    }
}
