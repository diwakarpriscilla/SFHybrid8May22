package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;
import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

public class AccountPage extends ProjectSpecificImplementation {
	
	public AccountPage(RemoteWebDriver driver, ExtentTest node) {
		this.driver = driver;
		this.node = node;
	}
	
	public AccountPage verifyAccountName(String AccountName) {
	WebElement Name = locateElementProp("AccountPage.AccountName.Xpath");
	String nameActual = getElementText(Name);
    System.out.println(nameActual);
    assert (nameActual).equals(AccountName) : "Verified Account Name " +AccountName;
    return this;
	}

}
