package pages;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;
import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

public class LeadsPage extends ProjectSpecificImplementation{
	
	public LeadsPage(RemoteWebDriver driver, ExtentTest node) {
		this.driver = driver;
		this.node = node;
	}
	
	public LeadsPage successMessage() {
		System.out.println("Completed");
		return this;
	}

}
