package pages;

import static org.testng.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

public class RecentlyViewedLeadPage extends ProjectSpecificImplementation {
	int lnameLead;
	
	public RecentlyViewedLeadPage(RemoteWebDriver driver, ExtentTest node) {
		this.driver = driver;
		this.node = node;
	}
	
	public NewLeadPage clickNewButton() throws InterruptedException {
	executorClick("RecentlyViewedLeadPage.NewButton.Xpath");
	Thread.sleep(2000);
	return new NewLeadPage(driver, node);
	}
	
	public RecentlyViewedLeadPage searchLeads(String Lname) throws InterruptedException {
	clearAndType((locateElementProp("RecentlyViewedLeadPage.SearchLead.Xpath")),Lname);
    appendEnter(locateElementProp("RecentlyViewedLeadPage.SearchLead.Xpath"));
	Thread.sleep(2000);
	return this;
	}
	
	public RecentlyViewedLeadPage clickDropdown() throws InterruptedException { 
	executorClick("RecentlyViewedLeadPage.ClickDropDown.Xpath");
	Thread.sleep(2000);
	return this;
	}
	
	public EditLeadPage clickEditDropDown() throws InterruptedException {
	executorClick("RecentlyViewedLeadPage.ClickEditDropDown.Xpath");
	Thread.sleep(2000);
	return new EditLeadPage(driver, node);
	}
	
	public RecentlyViewedLeadPage verifyResult(String fName, String lName) {
	WebDriverWait wait = new WebDriverWait(driver,20);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("RecentlyViewedLeadPage.ToastMessage.Xpath"))));
	String actualResult = driver.findElement(By.xpath(prop.getProperty("RecentlyViewedLeadPage.ToastMessage.Xpath"))).getText();
	System.out.println(actualResult);
		
	//String expectedResult = "Lead \"Mr. Ganesh Kumar\" was saved.";
	String expectedResult = "Lead \"Mr. "+fName+" "+lName+"\" was saved.";
	System.out.println(expectedResult);
		
	SoftAssert softAssert = new SoftAssert();
	softAssert.assertEquals(actualResult, expectedResult);
		
	if(actualResult .equalsIgnoreCase(expectedResult)) {
	    System.out.println("Lead creation is successful");
	 }else {
	    System.out.println("Lead Creation is NOT successful");
	     }
		softAssert.assertAll();
		return this;
		}
	
	public RecentlyViewedLeadPage countLeads(String lName) {
		List<WebElement> findLeadsLName = driver.findElements(By.xpath(prop.getProperty("RecentlyViewedLeadPage.FindLeadName.Xpath")));
		lnameLead = findLeadsLName.size();
		return this;
	}
	
	public RecentlyViewedLeadPage clickDeleteLead() throws InterruptedException {
	executorClick("RecentlyViewedLeadPage.Delete.Xpath");
	Thread.sleep(2000);
	return this;
	}
	
	public RecentlyViewedLeadPage clickDeleteButton() {
	executorClick("RecentlyViewedLeadPage.DeleteButton.Xpath");
	return this;
	}
	
	public RecentlyViewedLeadPage verifyDeletion(String leadName) throws InterruptedException {
		boolean flag=false;
		WebDriverWait wait = new WebDriverWait(driver,20);
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("RecentlyViewedLeadPage.ToastMessage.Xpath"))));
		String actualResult = driver.findElement(By.xpath(prop.getProperty("RecentlyViewedLeadPage.ToastMessage.Xpath"))).getText();
		System.out.println(actualResult);
		Thread.sleep(2000);
		
		String expectedResult = "Lead \""+leadName+"\" was deleted. Undo";
		System.out.println(expectedResult);
		
		if(expectedResult .equalsIgnoreCase(actualResult)) {
	    	System.out.println("Lead is deleted successfully and verified : " +actualResult);
	    	flag =true;
	    	 } else {
	    		 System.out.println("Lead is NOT deleted successfully : " +actualResult);
	    		 flag = false;
	    	 }
		assertEquals(true, flag);
		return this;
	}

}
