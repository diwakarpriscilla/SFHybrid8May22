package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

public class NewCasePage extends ProjectSpecificImplementation{
	
	public NewCasePage(RemoteWebDriver driver, ExtentTest node) {
		this.driver = driver;
		this.node = node;
	}
	
	public NewCasePage typeContact(String contact) throws InterruptedException {
	//driver.findElement(By.xpath(prop.getProperty("NewCasePage.SearchContacts.Xpath"))).click();
	//driver.findElement(By.xpath(prop.getProperty("NewCasePage.SearchContacts.Xpath"))).sendKeys(contact);
	click(locateElementProp("NewCasePage.SearchContacts.Xpath"));
	clearAndType((locateElementProp("NewCasePage.SearchContacts.Xpath")),contact);
	Thread.sleep(500);
	driver.findElement(By.xpath("//div[@title='"+contact+"']")).click();
	Thread.sleep(2000);
	return this;
	}
	
	public NewCasePage selectNone() { 
	//driver.findElement(By.xpath(prop.getProperty("NewCasePage.Status.Xpath"))).click();
	click(locateElementProp("NewCasePage.Status.Xpath"));
	executorClick("NewCasePage.Status.Xpath");
	/*
	 * WebElement none_status =
	 * driver.findElement(By.xpath(prop.getProperty("NewCasePage.Status.Xpath")));
	 * driver.executeScript("arguments[0].click();", none_status);
	 * executorClick("EditAccountPage.UpsellNo.Xpath");
	 */
    return this;
	}
	
	public NewCasePage enterSubject(String Subject) {  
    //driver.findElement(By.xpath(prop.getProperty("NewCasePage.Subject.Xpath"))).sendKeys(Subject);
    clearAndType((locateElementProp("NewCasePage.Subject.Xpath")),Subject);
	return this;
	}

	public NewCasePage enterDescription(String Description) {
    //driver.findElement(By.xpath(prop.getProperty("NewCasePage.Description.Xpath"))).sendKeys(Description);
    clearAndType((locateElementProp("NewCasePage.Description.Xpath")),Description);
    return this;
	}
	
	public NewCasePage clickSave() {
    //driver.findElement(By.xpath(prop.getProperty("NewCasePage.SaveButton.Xpath"))).click();
    click(locateElementProp("NewCasePage.SaveButton.Xpath"));
    return this;
	}
	
	public NewCasePage verifyErrorMessage(String errorMessageExpected) {
	//String errorMessageActual = driver.findElement(By.xpath(prop.getProperty("NewCasePage.ErrorMessage.Xpath"))).getText();
	String errorMessageActual = getElementText(locateElementProp("NewCasePage.ErrorMessage.Xpath"));
	System.out.println(errorMessageActual);
	    
	Boolean verifyMessage = errorMessageActual .contains(errorMessageExpected);
	SoftAssert softAssert = new SoftAssert();
	softAssert.assertTrue(verifyMessage);
		
	if(errorMessageActual .contains(errorMessageExpected)) {
	 System.out.println("Mandatory fields Verified");
	  } else {
	 System.out.println("Mandatory field testing failed");
	 System.out.println("Actual Error Message " +errorMessageActual);
	 System.out.println("Expected Error Message "+errorMessageExpected);
	  }
	softAssert.assertAll();
	return this;
	}
}
