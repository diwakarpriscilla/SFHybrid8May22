package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

public class NewLeadPage extends ProjectSpecificImplementation {
	
	public NewLeadPage(RemoteWebDriver driver, ExtentTest node) {
		this.driver = driver;
		this.node = node;
	}

	public NewLeadPage selectSalutation() {
	executorClick("NewLeadPage.Salutation.Xpath");
    executorClick("NewLeadPage.SalutationMr.Xpath");
	return this;
	}
	
	public NewLeadPage enterLastname(String Surname) {
	clearAndType((locateElementProp("NewLeadPage.LastName.Xpath")),Surname);
	return this;
	}
	
	public NewLeadPage enterCompanyName(String companyName) {
	clearAndType((locateElementProp("NewLeadPage.Company.Xpath")),companyName);
	return this;
	}
	
	public NewLeadPage clickSave() {
	click(locateElementProp("NewLeadPage.SaveEdit.Xpath"));
	return this;
	}
	
	public LeadsPage verification(String Surname) {
		WebDriverWait wait = new WebDriverWait(driver,20);
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("NewLeadPage.ToastMessage.Xpath"))));
	    String actualResult = getElementText(locateElementProp("NewLeadPage.ToastMessage.Xpath"));
		System.out.println(actualResult);
		
		String expectedResult = "Lead \"Mr. "+Surname+"\" was created.";
		System.out.println(expectedResult);
		
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(actualResult, expectedResult);
		
		if(actualResult .equalsIgnoreCase(expectedResult)) {
	    	System.out.println("Lead creation is successful");
	    }else {
	    	System.out.println("Lead Creation is NOT successful");
	    }
		softAssert.assertAll();
		return new LeadsPage(driver, node);
	}
	
	public NewLeadPage enterFirstName(String fName) {
	clearAndType((locateElementProp("NewLeadPage.FirstName.Xpath")),fName);
	return this;
	}
	
	public NewLeadPage mandatoryFieldVerification(String expectedResult) {
	String actualResult = getElementText(locateElementProp("NewLeadPage.ActualResultMandatory.Xpath"));
	System.out.println(actualResult);
	SoftAssert softAssert = new SoftAssert();
	softAssert.assertEquals(actualResult, expectedResult);
	softAssert.assertAll();
	return this;
	}
}
