package pages;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;
import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

public class EditContactPage extends ProjectSpecificImplementation {

	public EditContactPage(RemoteWebDriver driver, ExtentTest node) {
		this.driver = driver;
		this.node = node;
	}
	
	public EditContactPage enterTitle(String title) {
	clearAndType(locateElement("xpath","//input[@name='Title']"), title);
	return this;
	}
	
	public EditContactPage enterBirthday() throws InterruptedException {
	click(locateElement("xpath","//input[@name='Birthdate']"));
	clearAndType(locateElement("xpath","//input[@name='Birthdate']"), "7/21/1990");
	Thread.sleep(1000);
	return this;
	}
	
	public EditContactPage enterLeadSource() {
	executorClick("EditContactPage.LeadSource.Xpath");
	executorClick("EditContactPage.LeadSourceList.Xpath");
	return this;
	}
	
	public EditContactPage enterLeadSourcePartner() {
	executorClick("EditContactPage.LeadSource.Xpath");
	executorClick("EditContactPage.LeadSourceList2.Xpath");
	return this;
	}

	public EditContactPage enterLevel() throws InterruptedException {
	Thread.sleep(1000);
	executorClick("EditContactPage.Level.Xpath");
	executorClick("EditContactPage.LevelTer.Xpath");
	return this;
	}
	
	public EditContactPage mailingStreet(String mailStreet) {
	clearAndType(locateElement("xpath","//textarea[@name='street']"), mailStreet);
	return this;
	}
	
	public EditContactPage mailingCity(String mailCity) {
	clearAndType(locateElement("xpath","//input[@name='city']"), mailCity);
	return this;
	}
	
	public EditContactPage mailPostalCode(String mailPostalCode) {
	clearAndType(locateElement("xpath","//input[@name='postalCode']"), mailPostalCode);
	return this;
	}
	
	
	public EditContactPage mailProvince(String mailProvince) {
	clearAndType(locateElement("xpath","//input[@name='province']"), mailProvince);
	return this;
	}	
	
	public EditContactPage mailCountry(String mailCountry) {
	clearAndType(locateElement("xpath","//input[@name='country']"), mailCountry);
	return this;
	}
	
	public EditContactPage enterPhone(String phone) {
	clearAndType(locateElement("xpath","//input[@name='Phone']"), phone);
	return this;
	}
	
	public RecentlyViewedContactsPage clickSave() {
	click(locateElement("xpath","//button[@name='SaveEdit']"));
	return new RecentlyViewedContactsPage(driver, node);
	}
	
	public EditContactPage updateEmail(String email) {
	clearAndType(locateElement("xpath","//input[@name='Email']"), email);
	return this;
	}
}
