package pages;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;
import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

public class RecentlyViewedContactsPage extends ProjectSpecificImplementation {

	public RecentlyViewedContactsPage (RemoteWebDriver driver, ExtentTest node) {
		this.driver = driver;
		this.node = node;
	}
	
	public RecentlyViewedContactsPage sizeOfContacts() throws InterruptedException {
		try {
		    long lastHeight = (long) ((JavascriptExecutor) driver).executeScript("return document.body.scrollHeight");

		    while (true) {
		        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight);");
		        Thread.sleep(2000);

		        long newHeight = (long) ((JavascriptExecutor) driver).executeScript("return document.body.scrollHeight");
		        if (newHeight == lastHeight) {
		            break;
		        }
		        lastHeight = newHeight;
		    }
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
		int countContacts = locateElements("xpath", "//a[@data-refid='recordId' and contains(@data-recordid, '0032')]").size();
		System.out.println("No.of Existing Contacts :" +countContacts);
		Thread.sleep(2000);
		return this;
	}
	
	public RecentlyViewedContactsPage searchContacts(String conName) throws InterruptedException {
		clearAndType((locateElementProp("RecentlyViewedContactsPage.SearchContact.Xpath")),conName);
	    appendEnter(locateElementProp("RecentlyViewedContactsPage.SearchContact.Xpath"));
		Thread.sleep(2000);
		return this;
		}

	public RecentlyViewedContactsPage clickDropdown() throws InterruptedException { 
	executorClick("RecentlyViewedContactsPage.ClickDropDown.Xpath");
	Thread.sleep(2000);
	return this;
	}
	
	public EditContactPage clickEditDropDown() throws InterruptedException {
	executorClick("RecentlyViewedContactsPage.ClickEditDropDown.Xpath");
	Thread.sleep(2000);
	return new EditContactPage(driver, node);
	}
	
	public RecentlyViewedContactsPage clickDeletetDropDown() throws InterruptedException {
	executorClick("RecentlyViewedContactsPage.ClickDeleteDropDown.Xpath");
	Thread.sleep(2000);
	return this;
	}
	
	public RecentlyViewedContactsPage clickDeleteButton() throws InterruptedException {
	click(locateElementProp("RecentlyViewedContactsPage.DeleteButton.Xpath"));
	Thread.sleep(2000);
	return this;
	}
	
	public RecentlyViewedContactsPage verifySave(String contactName) throws InterruptedException {
	boolean flag=false;
	WebDriverWait wait = new WebDriverWait(driver,20);
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("RecentlyViewedContactsPage.ToastMessage.Xpath"))));
	String actualResult = driver.findElement(By.xpath(prop.getProperty("RecentlyViewedContactsPage.ToastMessage.Xpath"))).getText();
	System.out.println(actualResult);
	Thread.sleep(2000);
	
	String expectedResult = "Contact \"Mr. "+contactName+"\" was saved.";
	System.out.println(expectedResult);
	
	if(expectedResult .equalsIgnoreCase(actualResult)) {
    	System.out.println("Contact is saved successfully and verified : " +actualResult);
    	flag =true;
    	 } else {
    		 System.out.println("Contact is NOT saved successfully : " +actualResult);
    		 flag = false;
    	 }
	assertEquals(true, flag);
	return this;
	}
	
	public RecentlyViewedContactsPage verifyPhoneNumber(String phone) {
	String actualPhoneNum = getElementText(locateElement("xpath","//span[@dir='ltr']"));
	System.out.println(actualPhoneNum);
    String onlyPhoneNum = actualPhoneNum.replaceAll("\\D", "");
    System.out.println(onlyPhoneNum);
    assert (phone).equals(onlyPhoneNum) : "Phone number is correct. Edit is successful " +onlyPhoneNum;
	return this;
	}
	
	public RecentlyViewedContactsPage printEmail() {
	String Email = getElementText(locateElement("xpath","//a[@data-aura-class='emailuiFormattedEmail']"));
	System.out.println("Email registered is " +Email);
	return this;
	}
	
	public RecentlyViewedContactsPage verifyDelete(String contactName) {
	String expectedResult = "Contact \""+contactName+"\" was deleted. Undo";
	System.out.println(expectedResult);
	WebDriverWait wait = new WebDriverWait(driver,20);
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("RecentlyViewedContactsPage.ToastMessage.Xpath"))));
    String actualResult = getElementText(locateElementProp("RecentlyViewedContactsPage.ToastMessage.Xpath"));
	System.out.println(actualResult);
	if(expectedResult .equalsIgnoreCase(actualResult)) {
    	System.out.println("Contact deleted successfully and verified : " +actualResult);
    	 } else {
    		 System.out.println("Contact is NOT deleted successfully : " +actualResult);	 
    		 System.out.println("Actual :" +actualResult);
 	    	 System.out.println("Expected :" +expectedResult);
    	 }
	return this;
	}
}
