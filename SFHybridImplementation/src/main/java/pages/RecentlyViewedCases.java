package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;
import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

public class RecentlyViewedCases extends ProjectSpecificImplementation {
	
	public RecentlyViewedCases(RemoteWebDriver driver, ExtentTest node) {
		this.driver = driver;
		this.node = node;
	}
	
	public NewCasePage clickNewButton() throws InterruptedException {
	//driver.findElement(By.xpath(prop.getProperty("RecentlyViewedCases.NewButton.Xpath"))).click();
	executorClick("RecentlyViewedCases.NewButton.Xpath");
	Thread.sleep(4000);
	return new NewCasePage(driver, node);
	}

}
