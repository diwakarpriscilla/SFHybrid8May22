package pages;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Parameters;

import com.aventstack.extentreports.ExtentTest;
import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

public class LoginPage extends ProjectSpecificImplementation{
	
	public LoginPage(RemoteWebDriver driver, ExtentTest node) {
		this.driver = driver;
		this.node = node;
	}

	public LoginPage enterUsername(String userLoginName) throws InterruptedException, IOException {
	WebElement user_name = locateElement("id","username");
	clearAndType(user_name, userLoginName);
	Thread.sleep(1000);
	return this;
	}

	public LoginPage enterPassword(String userLoginPassword) throws InterruptedException { 
	WebElement password_name = locateElement("id","password");	
	clearAndType(password_name, userLoginPassword);
	Thread.sleep(1000);
	return this;
	}
	
	public HomePage clickLoginButton() throws InterruptedException {
	WebElement Login_Button = locateElement("id","Login");
	click(Login_Button);
	
	Thread.sleep(1000);
	String pageTitle = driver.getTitle();
	String title = "Developer Edition";
	if(pageTitle .contains(title)) {
		WebElement page_Title = locateElement("class","switch-to-lightning");
		click(page_Title);
		}
	return new HomePage(driver, node);
	}
	
}
