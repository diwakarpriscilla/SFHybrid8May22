package pages;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;
import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

public class HomePage extends ProjectSpecificImplementation {
	
	public HomePage(RemoteWebDriver driver, ExtentTest node) {
		this.driver = driver;
		this.node = node;
	}
	
	public HomePage clickToggleMenu() throws InterruptedException {
	click(locateElement("xpath", "//div[@class='slds-icon-waffle']"));
	Thread.sleep(4000);
	return this;
	}
	
	public HomePage clickViewAll() throws InterruptedException {
	executorClick("HomePage.ViewAll.Xpath");
	Thread.sleep(2000);
	return this;
	}
	
	public HomePage clickSales() throws InterruptedException {
	click(locateElementProp("HomePage.Sales.Xpath"));
	Thread.sleep(1000);
	return this;
	}
	
	public RecentlyViewedAccountPage clickAccountTab() {
	executorClick("HomePage.Accounts.Xpath");
	return new RecentlyViewedAccountPage(driver, node);
	}
	
	public RecentlyViewedLeadPage clickLeadTab() throws InterruptedException {
	executorClick("HomePage.LeadsTab.Xpath");
	Thread.sleep(2000);
	return new RecentlyViewedLeadPage(driver, node);
	}

	public NewTaskPage clickTasksTab() throws InterruptedException {
	executorClick("HomePage.TasksTab.Xpath");
	Thread.sleep(2000);
	return new NewTaskPage(driver, node);
	}	
	public RecentlyViewedWorkTypePage clickWorkTypes() throws InterruptedException {
	WebElement selectWorkTypes = driver.findElement(By.xpath(prop.getProperty("HomePage.selectWorkTypes.Xpath")));
	driver.executeScript("arguments[0].scrollIntoView();", selectWorkTypes);
	click(locateElementProp("HomePage.selectWorkTypes.Xpath"));
	Thread.sleep(2000);
	return new RecentlyViewedWorkTypePage(driver, node);
	}
	
	public HomePage clickSVG() throws InterruptedException{
	Thread.sleep(8000);
	WebDriverWait wait = new WebDriverWait(driver,20);
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("HomePage.SVG.Xpath"))));
	WebElement svg = driver.findElement(By.xpath(prop.getProperty("HomePage.SVG.Xpath")));
	Actions action = new Actions(driver);
	action.click(svg).build().perform();
	Thread.sleep(8000);
	return new HomePage(driver, node);
	}
		
	public NewContactPage clickNewContact() throws InterruptedException{
	executorClick("HomePage.NewContact.Xpath");
	Thread.sleep(8000);
	return new NewContactPage(driver, node);
	}
		
	public HomePage enterAccountName(String AccountName) throws InterruptedException {
	clearAndType((locateElementProp("HomePage.AccountName.Xpath")),AccountName);
    appendEnter(locateElementProp("HomePage.AccountName.Xpath"));
	Thread.sleep(8000);
	return this;
	}
		
	public NewContactPage clickAccountSave() throws InterruptedException {
	executorClick("HomePage.SaveButton.Xpath");
	return new NewContactPage(driver, node);
	}
	
	public RecentlyViewedCases showMore() throws InterruptedException {
	executorClick("HomePage.ShowMore.Xpath");
	Thread.sleep(2000);
	executorClick("HomePage.Cases.Xpath");
	Thread.sleep(4000);	
	return new RecentlyViewedCases(driver, node);
	}
	
	public RecentlyViewedContactsPage clickContacts() throws InterruptedException {
	Thread.sleep(1000);
	executorClick("HomePage.Contacts.Xpath");
	Thread.sleep(2000);	
	return new RecentlyViewedContactsPage(driver, node);
	}
}
