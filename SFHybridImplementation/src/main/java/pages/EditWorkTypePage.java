package pages;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;
import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

public class EditWorkTypePage extends ProjectSpecificImplementation {
	
	public EditWorkTypePage(RemoteWebDriver driver, ExtentTest node) {
		this.driver = driver;
		this.node = node;
	}
	
	public EditWorkTypePage enterTimeFrameStart(String StartTime) throws InterruptedException {
	clearAndType((locateElementProp("EditWorkTypePage.TimeframeStart.Xpath")),StartTime);
	Thread.sleep(1000);
	return this;
	}
	
	public EditWorkTypePage enterTimeFramEnd(String EndTime) throws InterruptedException { 
	clearAndType((locateElementProp("EditWorkTypePage.TimeframeEnd.Xpath")),EndTime);
	Thread.sleep(1000);
	return this;
	}
	
	public RecentlyViewedWorkTypePage clickSave() {
	click(locateElementProp("EditWorkTypePage.Save.Xpath"));
	return new RecentlyViewedWorkTypePage(driver, node);
	}

}
