package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;
import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

public class NewAccountPage extends ProjectSpecificImplementation {
	
	public NewAccountPage(RemoteWebDriver driver, ExtentTest node) {
		this.driver = driver;
		this.node = node;
	}
	
	public NewAccountPage enterAccountName(String AccountName) throws InterruptedException {
    //driver.findElement(By.xpath("//input[@name='Name']")).sendKeys(AccountName);
    driver.findElement(By.xpath(prop.getProperty("NewAccountPage.AccountName.Xpath"))).sendKeys(AccountName);
    Thread.sleep(2000);
    return this;
	 }
	
	public NewAccountPage selectOwnership(){
    WebElement Ownership = driver.findElement(By.xpath(prop.getProperty("NewAccountPage.Ownership.Xpath")));
    //WebElement Ownership = driver.findElement(By.xpath("(//button[@class='slds-combobox__input slds-input_faux slds-combobox__input-value'])[3]"));
    driver.executeScript("arguments[0].click();", Ownership);
    return this;
	 }
    
    public AccountPage clickSave(){
    	driver.findElement(By.xpath(prop.getProperty("NewAccountPage.SaveEdit.Xpath"))).click();
    	//driver.findElement(By.xpath("//button[@name='SaveEdit']")).click();
    	return new AccountPage(driver, node);
    }
    

}
