package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

public class NewTaskPage extends ProjectSpecificImplementation {
	
	public NewTaskPage(RemoteWebDriver driver, ExtentTest node) {
		this.driver = driver;
		this.node = node;
	}
	
	public NewTaskPage clickTasksTab() throws InterruptedException {
	executorClick("NewTaskPage.TaskMenu.Xpath");
	Thread.sleep(3000);
	return this;
	}
	
	public NewTaskPage clickNewTask() throws InterruptedException {
	executorClick("NewTaskPage.NewTask.Xpath");
	Thread.sleep(1000);
	return this;
	}
	
	public NewTaskPage clickRecentlyViewed() throws InterruptedException {
	executorClick("NewTaskPage.RecentlyViewed.Xpath");
	Thread.sleep(1000);
	return this;
	}
	
	public NewTaskPage search(String subjectName) throws InterruptedException {
	clearAndType(locateElementProp("NewTaskPage.Search.Xpath"), subjectName);
	Thread.sleep(1000);
	appendEnter(locateElementProp("NewTaskPage.Subject.Xpath"));
	Thread.sleep(1000);
	return this;
	}
	
	public NewTaskPage enterSubject(String subjectName) throws InterruptedException {
	clearAndType(locateElementProp("NewTaskPage.Subject.Xpath"), subjectName);
	Thread.sleep(1000);
	return this;
	}
	
	public NewTaskPage clickStatus() throws InterruptedException {
	executorClick("NewTaskPage.Status.Xpath");
	Thread.sleep(1000);
	return this;
	}
	
	public NewTaskPage clickStatusOptions() throws InterruptedException {
	executorClick("NewTaskPage.StatusOption.Xpath");
	Thread.sleep(1000);
	return this;
	}
	
	public NewTaskPage enterContact(String contactName) throws InterruptedException {
	clearAndType(locateElementProp("NewTaskPage.Contact.Xpath"),contactName);
	Thread.sleep(1000);
	appendKeyDown(locateElementProp("NewTaskPage.Contact.Xpath"));
	Thread.sleep(1000);
	return this;
	}
	
	public NewTaskPage clickSave() throws InterruptedException {
	executorClick("NewTaskPage.SaveButton.Xpath");
	Thread.sleep(1000);
	return this;
	}
	
	public NewTaskPage verifyCreation(String subjectName) {
	WebDriverWait wait = new WebDriverWait(driver,20);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("NewTaskPage.ToastMessage.Xpath"))));
	String actualResult = getElementText(locateElementProp("NewTaskPage.ToastMessage.Xpath"));
	String expectedResult = "Task "+subjectName+" was created.";
	SoftAssert softAssert = new SoftAssert();
	softAssert.assertEquals(actualResult, expectedResult);
	
		if(expectedResult .equalsIgnoreCase(actualResult)) {
	    	System.out.println("New Task created successfully and verified : " +actualResult);
	    	 } else {
	    		 System.out.println("New Task is NOT created successfully :");
	    		 System.out.println("Actual :" +actualResult);
	 	    	 System.out.println("Expected :" +expectedResult);
	    	 }
	softAssert.assertAll();
	return this;
	}
	
	public NewTaskPage clickArrowButton() throws InterruptedException {
	executorClick("NewTaskPage.ArrowButton.Xpath");
	Thread.sleep(1000);
	return this;
	}
	
	public NewTaskPage clickEditDropDown() throws InterruptedException {
	executorClick("NewTaskPage.EditDropdown.Xpath");
	Thread.sleep(1000);
	return this;
	}
	
	public NewTaskPage clickDueDate() throws InterruptedException {
	executorClick("NewTaskPage.DueDate.Xpath");
	Thread.sleep(1000);
	return this;
		}
	
	public NewTaskPage clickDueDateToday() throws InterruptedException {
	executorClick("NewTaskPage.DueDateToday.Xpath");
	Thread.sleep(1000);
	return this;
	}
}
