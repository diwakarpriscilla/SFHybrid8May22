package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

public class RecentlyViewedWorkTypePage extends ProjectSpecificImplementation {
	
	public String workTypeNameEdit, WorkTypeName;
	
	public RecentlyViewedWorkTypePage(RemoteWebDriver driver, ExtentTest node) {
		this.driver = driver;
		this.node = node;
	}
	
	public NewWorkTypePage clickNewButton() throws InterruptedException {
	click(locateElementProp("RecentlyViewedWorkTypePage.NewButton.Xpath"));
	Thread.sleep(2000);
	return new NewWorkTypePage(driver, node);
	}
	
	public RecentlyViewedWorkTypePage verifyCreation(String WorkTypeName) {
	WebDriverWait wait = new WebDriverWait(driver,20);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("RecentlyViewedWorkTypePage.ToastMessage.Xpath"))));
	String actualResult = getElementText(locateElementProp("RecentlyViewedWorkTypePage.ToastMessage.Xpath"));
	String expectedResult = "Work Type \""+WorkTypeName+"\" was created.";
	SoftAssert softAssert = new SoftAssert();
	softAssert.assertEquals(actualResult, expectedResult);
	
		if(expectedResult .equalsIgnoreCase(actualResult)) {
	    	System.out.println("Work Type created successfully and verified : " +actualResult);
	    	 } else {
	    		 System.out.println("Work Type is NOT created successfully :" +actualResult);
	    		 System.out.println("Actual :" +actualResult);
	 	    	 System.out.println("Expected :" +expectedResult);
	    	 }
	softAssert.assertAll();
	return this;
	}
	
	
	public RecentlyViewedWorkTypePage searchWorkType(String WorkTypeName) throws InterruptedException{
	Thread.sleep(2000);
	clearAndType((locateElementProp("RecentlyViewedWorkTypePage.WorkTypeName.Xpath")),WorkTypeName);
    appendEnter(locateElementProp("RecentlyViewedWorkTypePage.WorkTypeName.Xpath"));
	Thread.sleep(6000);
	return this; 
	}
	 
	
	public RecentlyViewedWorkTypePage clickArrowButton() throws InterruptedException {
	click(locateElementProp("RecentlyViewedWorkTypePage.ArrowButton.Xpath"));
	Thread.sleep(2000);
	return this;
	}
	
	public EditWorkTypePage clickEditDropDown() throws InterruptedException{
	click(locateElementProp("RecentlyViewedWorkTypePage.EditDropdown.Xpath"));
	Thread.sleep(2000);
	return new EditWorkTypePage(driver, node);
	}
	
	public RecentlyViewedWorkTypePage clickDeleteDropDown() throws InterruptedException {
	click(locateElementProp("RecentlyViewedWorkTypePage.DeleteDropdown.Xpath"));
	Thread.sleep(2000);
	return this;
	}
			
	public RecentlyViewedWorkTypePage clickDeleteButton() throws InterruptedException {
	click(locateElement("xpath", "//span[text()='Delete']"));
	Thread.sleep(2000);
	return this;
	}
	
	public RecentlyViewedWorkTypePage verifyEdit(String WorkTypeName) {
	WebDriverWait wait = new WebDriverWait(driver,20);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("RecentlyViewedWorkTypePage.ToastMessage.Xpath"))));
	String actualResult = getElementText(locateElementProp("RecentlyViewedWorkTypePage.ToastMessage.Xpath"));
	String expectedResult= "Work Type \""+WorkTypeName+ "\" was saved.";
	SoftAssert softAssert = new SoftAssert();
	softAssert.assertEquals(actualResult, expectedResult);
	if(expectedResult .equalsIgnoreCase(actualResult)) {
	    System.out.println("Work Type saved successfully and verified : " +actualResult);
	     } else {
	    	System.out.println("Work Type is NOT saved successfully : " );
	    	System.out.println("Actual :" +actualResult);
	    	System.out.println("Expected :" +expectedResult);	 
	     }
	softAssert.assertAll();
	return this;
	}
	
	public RecentlyViewedWorkTypePage verifyDelete(String WorkTypeName) {
	String expectedResult = "Work Type \""+WorkTypeName+"\" was deleted. Undo";
	System.out.println(expectedResult);
	WebDriverWait wait = new WebDriverWait(driver,20);
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("RecentlyViewedWorkTypePage.ToastMessageDelete.Xpath"))));
    String actualResult = getElementText(locateElementProp("RecentlyViewedWorkTypePage.ToastMessageDelete.Xpath"));
	System.out.println(actualResult);
	if(expectedResult .equalsIgnoreCase(actualResult)) {
    	System.out.println("Work Type deleted successfully and verified : " +actualResult);
    	 } else {
    		 System.out.println("Work Type is NOT deleted successfully : " +actualResult);	 
    		 System.out.println("Actual :" +actualResult);
 	    	 System.out.println("Expected :" +expectedResult);
    	 }
	return this;
	}

}
