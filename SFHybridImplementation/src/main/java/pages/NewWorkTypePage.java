package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;
import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

public class NewWorkTypePage extends ProjectSpecificImplementation {
	
	public NewWorkTypePage(RemoteWebDriver driver, ExtentTest node) {
		this.driver = driver;
		this.node = node;
	}
	
	public NewWorkTypePage typeWorkTypeName(String WorkTypeName) {
	clearAndType((locateElementProp("NewWorkTypePage.WorkTypeName.Xpath")),WorkTypeName);
	return this;		
	}
			
	public NewWorkTypePage typeDescription(String Description) {
	clearAndType((locateElementProp("NewWorkTypePage.TypeDescription.Xpath")),Description);
	return this;
	}
			
	public NewWorkTypePage newOperatingHours(String Shift) {
	clearAndType((locateElementProp("NewWorkTypePage.SearchOperatingHours.Xpath")),Shift);
	click(locateElementProp("NewWorkTypePage.UKSHIFT.Xpath"));
	return this;
	}
			
	public NewWorkTypePage enterEstimatedDuration(String Duration) {
	clearAndType((locateElementProp("NewWorkTypePage.EstimatedDuration.Xpath")),Duration);
	return this;
	}		
	
	public RecentlyViewedWorkTypePage clickSave() {
	click(locateElementProp("NewWorkTypePage.SaveButton.Xpath"));
	return new RecentlyViewedWorkTypePage(driver, node);
	}

}
