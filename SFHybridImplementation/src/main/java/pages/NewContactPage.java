package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;

public class NewContactPage extends ProjectSpecificImplementation {
	
	public NewContactPage(RemoteWebDriver driver, ExtentTest node) {
		this.driver = driver;
		this.node = node;
	}
	
	public NewContactPage selectSalutation() throws InterruptedException {
	//WebElement salutation = driver.findElement(By.xpath(prop.getProperty("NewContactPage.Salutation.Xpath")));
	//driver.executeScript("arguments[0].click();", salutation);
	executorClick("NewContactPage.Salutation.Xpath");
	Thread.sleep(8000);
	executorClick("NewContactPage.Mr.Xpath");
	//WebElement mr =driver.findElement(By.xpath(prop.getProperty("NewContactPage.Mr.Xpath")));
	//driver.executeScript("arguments[0].click();", mr);
	Thread.sleep(1000);
	return this;
	}

	public NewContactPage enterLastname(String Surname) {
		/*
		 * WebElement lastName =
		 * driver.findElement(By.xpath(prop.getProperty("NewContactPage.LastName.Xpath")
		 * )); lastName.sendKeys(Surname, Keys.ENTER);
		 */
	clearAndType((locateElementProp("NewContactPage.LastName.Xpath")),Surname);
    appendEnter(locateElementProp("NewContactPage.LastName.Xpath"));
	return this;
	}

	public NewContactPage enterFirstname(String FirstName) {
		/*
		 * WebElement first_Name =
		 * driver.findElement(By.xpath(prop.getProperty("NewContactPage.FirstName.Xpath"
		 * ))); first_Name.sendKeys(FirstName, Keys.ENTER);
		 */
	clearAndType((locateElementProp("NewContactPage.FirstName.Xpath")),FirstName);
    appendEnter(locateElementProp("NewContactPage.FirstName.Xpath"));
	return this;
	}

	public NewContactPage enterEmail(String Email) {
		/*
		 * WebElement emailfield =
		 * driver.findElement(By.xpath(prop.getProperty("NewContactPage.Email.Xpath")));
		 * driver.executeScript("arguments[0].value='"+Email+"';", emailfield);
		 */
	
	clearAndType((locateElementProp("NewContactPage.Email.Xpath")),Email);
	return this;
	}
	
	public NewContactPage searchAccount() throws InterruptedException{
		/*
		 * WebElement searchAccount = driver.findElement(By.xpath(prop.getProperty(
		 * "NewContactPage.SearchAccount.Xpath")));
		 * driver.executeScript("arguments[0].click();", searchAccount);
		 */
	executorClick("NewContactPage.SearchAccount.Xpath");
	Thread.sleep(2000);
	return this;
	}

	public HomePage newAccount() throws InterruptedException{
		/*
		 * WebElement newAccount = driver.findElement(By.xpath(prop.getProperty(
		 * "NewContactPage.CreateAccount.Xpath")));
		 * driver.executeScript("arguments[0].click();", newAccount);
		 */
	executorClick("NewContactPage.CreateAccount.Xpath");
	Thread.sleep(6000);
	return new HomePage(driver, node);
	}
	
	public NewContactPage verifyAccountSave(String AccountName) {
	String expectedResult = "Account \""+AccountName+"\" was created.";
	System.out.println(expectedResult);
	WebDriverWait wait = new WebDriverWait(driver,20);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("NewContactPage.Toast.Xpath"))));
	String actualResult = driver.findElement(By.xpath(prop.getProperty("NewContactPage.Toast.Xpath"))).getText();
	System.out.println(actualResult);
	
	SoftAssert softAssert = new SoftAssert();
	softAssert.assertEquals(actualResult, expectedResult);
	
	if(expectedResult .equalsIgnoreCase(actualResult)) {
	 System.out.println("Account is created succccessfully and verified : " +actualResult);
	 } else {
	    	System.out.println("Account is NOT created successfully : ");
	    	System.out.println("Actual result: "+actualResult);
	    	System.out.println("Expected result: "+expectedResult);
	    	}
	softAssert.assertAll();
	return this;
	}

	public NewContactPage clickSaveContact() throws InterruptedException{
	Thread.sleep(8000);
	WebElement save = driver.findElement(By.xpath(prop.getProperty("NewContactPage.SaveButton.Xpath")));
	driver.executeScript("arguments[0].click();", save);
	return this;
	}

	public HomePage verifyContactSave(String FirstName, String Surname) {
	String expectedResult = "Contact "+FirstName +" " +Surname+ " was created.";
	System.out.println(expectedResult);
	WebDriverWait wait = new WebDriverWait(driver,20);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("NewContactPage.Toast.Xpath"))));
	String actualResult = driver.findElement(By.xpath(prop.getProperty("NewContactPage.Toast.Xpath"))).getText();
	System.out.println(actualResult);
	
	SoftAssert softAssert = new SoftAssert();
	softAssert.assertEquals(actualResult, expectedResult);
	
	if(expectedResult .equalsIgnoreCase(actualResult)) {
	 System.out.println("Contact is created succccessfully and verified : " +actualResult);
	 } else {
	    	System.out.println("Contact is NOT created successfully : ");
	    	System.out.println("Actual result: "+actualResult);
	    	System.out.println("Expected result: "+expectedResult);
	    	}
	softAssert.assertAll();
	return new HomePage(driver, node);
	}
}
