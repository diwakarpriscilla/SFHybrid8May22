package pages;

import static org.testng.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;
import com.salesforce.selenium.commonClass.ProjectSpecificImplementation;


public class RecentlyViewedAccountPage extends ProjectSpecificImplementation{
	
	public RecentlyViewedAccountPage(RemoteWebDriver driver, ExtentTest node) {
		this.driver = driver;
		this.node = node;
	}
	
	int accountNameCount, accountNameCount1;
	
	public NewAccountPage clickNewButton() {
    click(locateElementProp("RecentlyViewedAccountPage.NewButton.Xpath"));
    return new NewAccountPage(driver, node);
    }
	
	public RecentlyViewedAccountPage searchAccount(String AccountName) throws InterruptedException {
	clearAndType((locateElementProp("RecentlyViewedAccountPage.SearchAccount.Xpath")),AccountName);
    appendEnter(locateElementProp("RecentlyViewedAccountPage.SearchAccount.Xpath"));
	Thread.sleep(4000);
    return this;
	}
	
	public RecentlyViewedAccountPage clickDropdown() {
	executorClick("RecentlyViewedAccountPage.DropDown.Xpath");
    return this;
	}

    public EditAccountPage selectEdit() throws InterruptedException {
    click(locateElementProp("RecentlyViewedAccountPage.SelectEdit.Xpath"));
    Thread.sleep(2000);
    return new EditAccountPage(driver, node);
    }
    
    public RecentlyViewedAccountPage verifyPhoneNumber(String phone) {
    String pNum = getElementText(locateElementProp("RecentlyViewedAccountPage.PhoneNumber.XPath"));
    System.out.println(pNum);
    String onlyPhoneNum = pNum.replaceAll("\\D", "");
    System.out.println(onlyPhoneNum);
    assert (phone).equals(onlyPhoneNum) : "Phone number is correct. Edit is successful " +onlyPhoneNum;
    return this;
    }
    
    public RecentlyViewedAccountPage countAccount(String AccountName) throws InterruptedException {
    	accountNameCount = accountNameCount1;
        clearAndType((locateElementProp("RecentlyViewedAccountPage.SearchAccount.XPath")),AccountName);
        appendEnter(locateElementProp("RecentlyViewedAccountPage.SearchAccount.XPath"));
    	//driver.findElement(By.xpath(prop.getProperty("RecentlyViewedAccountPage.SearchAccount.XPath"))).clear();
    	//driver.findElement(By.xpath(prop.getProperty("RecentlyViewedAccountPage.SearchAccount.XPath"))).sendKeys(AccountName,Keys.ENTER);
	    Thread.sleep(2000);
	    List<WebElement> findAccountName = driver.findElements(By.xpath(prop.getProperty("RecentlyViewedAccountPage.FindAccount.XPath")));
		accountNameCount1 = findAccountName.size();
		System.out.println(accountNameCount);
		System.out.println(accountNameCount1);
		return this;
    }
    
    public RecentlyViewedAccountPage verification() {
    	int a = accountNameCount - accountNameCount1;
    	boolean flag = false;
    	boolean expected_flag = true;
		System.out.println(a);
		
		if(a==1) {
			System.out.println("Account deleted successfully");
			flag = true;
		} else {
			System.out.println("Account is NOT deleted");
			flag = false;
		}
		assertEquals(flag, expected_flag);
		return this;
    }
    
    public RecentlyViewedAccountPage selectDeleteDropdown() throws InterruptedException {
    click(locateElementProp("RecentlyViewedAccountPage.DeleteDropDown.XPath"));
	Thread.sleep(2000);
    return this;
        }
    
    public RecentlyViewedAccountPage clickDeleteButton() throws InterruptedException {
    click(locateElementProp("RecentlyViewedAccountPage.DeleteButton.XPath"));
	Thread.sleep(2000);
	return this;
    }	 
}
