package com.salesforce.selenium.commonClass;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import com.salesforce.selenium.base.SeleniumBase;

import utils.ReadExcel;



public class ProjectSpecificImplementation extends SeleniumBase {

	public String excelFileName;
	

	@DataProvider(name = "fetchData")
	public Object[][] fetchData() throws IOException {
		return ReadExcel.readData(excelFileName);
	}	

	@BeforeMethod
	@Parameters("url")
	public void beforeMethod(String website) throws IOException {
		FileInputStream fis = new FileInputStream("./src/main/resources/config.properties");
		
		//step2: Create object for Properties
		prop = new Properties();
					
		//step3: Load the properties
		prop.load(fis);
		
		driver = startApp("chrome", website);
		node = test.createNode(testCaseName);
	}

	@AfterMethod
	public void afterMethod() {
		close();
	}

}
